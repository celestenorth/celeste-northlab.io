* Quick Start

[[https://gitlab.com/celeste-north/celeste-north.gitlab.io/-/commits/trunk][https://gitlab.com/celeste-north/celeste-north.gitlab.io/badges/trunk/pipeline.svg]]

Assuming =DIR_PATH= is something like =~/Web=, get the repo by running
from the command line

#+begin_src shell
$ cd DIR_PATH
$ git clone https://gitlab.com/celeste-north/celeste-north.gitlab.io.git
#+end_src

Go into the repo, and install all dependecies

#+begin_src shell
$ cd celeste-north.gitlab.io
$ bundle install
#+end_src

To run a demo for development

#+begin_src shell
$ bundle exec rake demo
#+end_src

To view the live demo visit =http://localhost:4000= from your browser.
You can make changes to pretty much anything, except for =config.yml=,
without having to restart the demo. To stop the demo simply press
=Ctrl-c= on the keyboard.

To see a list with all available commands

#+begin_src shell
$ bundle exec rake
#+end_src

** Configuration

=_config.yml= holds the site configuration, as well as the =author=
profile details. Whenever we update =_config.yml= we must restart the
demo to see the changes. Check the documentation for more details.

** Styling

Anything listed inside =assets/styling/main.scss= is processed as a
single =CSS= file. For maintenance, all =SCSS= modules are under
=_scss/=, though.

Whenever you remove a file from =_scss/=, you must also
unlist it from =assets/styling/main.scss=.

Occassionally you may need to restart the demo for all the changes to
take effect.

* Domain name

Gitlab allows the use of custom domain names (purchased elsewhere) for
Gitlab pages. Follow the [[https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/][documentation]] whenever the domain needs to
change. Keep in mind that some domain name providers require the name to
end in a dot (=.=), though. That is:

- the =A= record should point to =example.com.= rather than =example.com=
- the =TXT= key should be =_gitlab-pages-verification-code.example.com.=
