# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "minitest/autorun"
require "minitest/pride"
require "rake"
require "tmpdir"

class TestRakefile < Minitest::Test
  make_my_diffs_pretty!
  $VERBOSE = nil # silence 'reinitialized constant' warning

  def setup
    rake = Rake.application
    rake.init
    rake.load_rakefile
    @task = Rake::Task
  end

  def test_publishes_to_given_directory
    skip("Too slow for local development") unless ENV["RAKEFILE_TESTS"]
    Dir.mktmpdir { |tmp|
      @task[:publish].reenable
      capture_subprocess_io { @task[:publish].invoke tmp }

      assert File.exist?("#{tmp}/index.html")
    }
  end

  def test_publishes_for_given_environment
    skip("Too slow for local development") unless ENV["RAKEFILE_TESTS"]
    Dir.mktmpdir { |tmp|
      env = "testing"
      @task[:publish].reenable
      out, _ = capture_subprocess_io { @task[:publish].invoke tmp, env}

      assert_match %r"#{env}"i, out
    }
  end

  def test_published_portfolio_to_default_directory
    skip("Too slow for local development") unless ENV["RAKEFILE_TESTS"]
    @task[:publish].reenable
    capture_subprocess_io { @task[:publish].invoke }

    assert File.exist?("#{PORTFOLIO_PATH}/index.html")
  end

  def test_removes_generated_portfolio_from_default_directory
    skip("Too slow for local development") unless ENV["RAKEFILE_TESTS"]
    @task[:publish].reenable
    capture_subprocess_io { @task[:publish].invoke }
    @task[:clobber].invoke

    refute Dir.exist?(PORTFOLIO_PATH)
  end

  def test_lists_all_task_via_help
    out, _ = capture_subprocess_io { @task[:help].execute }
    assert_match %r"rake help"i, out
  end

  def test_displays_help_by_default_outside_production
    old_env = ENV["JEKYLL_ENV"] ? ENV.delete("JEKYLL_ENV") : nil

    out, _ = capture_subprocess_io {
      @task[:default].reenable
      @task[:default].invoke
    }

    assert_match %r"rake help"i, out

    ENV["JEKYLL_ENV"] = old_env
  end

  def test_validates_site_assuming_default_settings
    skip
  end

  def test_validates_site_by_default_in_production
    skip
  end
end
